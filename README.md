# 银联支付项目环境搭建

## 一、银联在线支付对接

银联接口官方文档  
https://open.unionpay.com/ajweb/index

用户名称: itmayiedu  密码 15527339672w  
测试商户号：777290058152972  

使用账号登录网址，登录后选择在线网关支付产品，在测试参数处下载以下证书，放到D:/certs目录，可以在  
acp_sdk.properties配置文件进行更改目录。

测试证书5.1.0：敏感加密证书 商户私钥证书（签名使用,密码：000000）

测试证书5.0.0、6.0.0：商户私钥证书（签名使用,密码：000000） 银联公钥证书（签名使用）

## 二、项目启动

### 1、启动itmayiedu-shopp-eurekaserver工程  
访问http://localhost:8761/进入eureka控制台

### 2、启动imayiedu-shopp-pay工程
spring.application.name = pay-service

端口号:8081
  
db数据源配置
```
spring.datasource.driverClassName=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://com.bingqu.yun:3306/db_pay?characterEncoding=utf8
spring.datasource.username=root
spring.datasource.password=123456
```
redis配置
```
# Redis数据库索引（默认为0）
spring.redis.database=0  
# Redis服务器地址
spring.redis.host=com.bingqu.yun
# 集群下的服务器地址
#spring.redis.cluster.nodes=129.204.241.250:6381, 129.204.241.250:6382, 129.204.241.250:6383, 129.204.241.250:6384, 129.204.241.250:6385, 129.204.241.250:6386
# Redis服务器连接端口
spring.redis.port=6379  
# Redis服务器连接密码（默认为空）
spring.redis.password=
# 连接池最大连接数（使用负值表示没有限制）
spring.redis.jedis.pool.max-active=200
# 连接池最大阻塞等待时间（使用负值表示没有限制）
spring.redis.jedis.pool.max-wait=-1
# 连接池中的最大空闲连接
spring.redis.jedis.pool.max-idle=10
# 连接池中的最小空闲连接
spring.redis.jedis.pool.min-idle=0
# 连接超时时间（毫秒）
spring.redis.timeout=1000
```
### 3、启动imayiedu-shopp-pay-web工程
spring.application.name=pay-web

端口号:8080

## 三、测试支付接口(具体见ApiTest目录)

### 内网映射
登录https://natapp.cn/注册，下载natapp之后，配置config.ini  
authtoken=64e0e20204788646 
 
打开natapp工具获取公网域名adpjvp.natappfree.c，不过每次打开域名都会变化。
将回调URL配置在payment_type表中,具体见payment_type.sql  

也可以买natapp的付费隧道，这样域名可以设置成固定的。

### imayiedu-shopp-pay-web工程的token获取接口
http://localhost:8081/pay/addPayInfoToken
Content-Type: application/json

{
	"typeId":"1",
	"orderId":"20200900001",
	"price":"100",
	"source":"1",
	"state":"0",
	"platformorderId":"0000009",
	"payMessage":"**支付成功**"
}

### imayiedu-shopp-pay-web工程的支付网关接口
http://localhost:8080/payGateway?token=pay-17a5e1a4-76af-43e0-a5b7-cebec9b5a87b

为了方便，多次测试只要更改同个token的订单号即可，银联那边只会根据订单号来判断是否存在已成功的重复交易。

测试卡号： 6216261000000000018  
姓名： 全渠道  
手机号： 13552535506  	
密码： 123456	 		
证件号： 341126197709218366  
短信验证码： 111111

测试遇到的问题:  
payment_info表的paymessage的字段长度过短，导致更新报文字段时,SQL异常，将其更改为5000问题解决。

## 四、SQL脚本(具体见SQL目录)