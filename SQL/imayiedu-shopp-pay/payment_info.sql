create table payment_info (
  id bigint(20) not null AUTO_INCREMENT,
  typeid bigint(20) NOT NULL COMMENT '支付类型id(payment_type主键)',
  orderid varchar(500) NOT NULL COMMENT '订单编号',
  platformorderid varchar(500) COMMENT '第三方支付平台订单ID',
  price bigint(20) NOT NULL COMMENT '商品价格，单位:分',
  source varchar (10) NOT NULL COMMENT '渠道来源',
  state varchar (2) DEFAULT NULL COMMENT '状态 0待支付 1支付成功 2师父失败',
  paymessage varchar(5000) COMMENT '支付报文',
  txnTime datetime COMMENT '订单发起时间',
  created datetime NOT NULL,
  updated datetime NOT NULL,
  primary key (id)
) ENGINE = InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='支付信息表';

