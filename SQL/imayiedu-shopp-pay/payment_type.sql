create table payment_type (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  typename varchar(100) NOT NULL COMMENT '支付类型名称',
  fronturl varchar(500) NOT NULL COMMENT '同步回调URL',
  backurl varchar(500) NOT NULL COMMENT '异步回调URL',
  merchantid varchar(150) NOT NULL COMMENT '商户id',
  created datetime NOT NULL,
  updated datetime NOT NULL,
  primary key (id)
) ENGINE = InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='支付类型表';

INSERT INTO `payment_type` VALUES (1, 'yinlianPay', 'http://adpjvp.natappfree.cc/pay/callback/syn', 'http://adpjvp.natappfree.cc/pay/callback/asyn', '777290058152972', '2020-9-12 18:14:36', '2020-9-12 18:14:45');
